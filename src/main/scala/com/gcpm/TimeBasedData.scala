package com.gcpm

import java.util.Date

trait TimeBasedData {
  def getDate(): Date
}
