package com.gcpm

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer


trait AkkaConfig {
  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
}
