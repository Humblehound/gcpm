package com.gcpm


import java.util.Date

import com.gcpm.gamedata.{GameData, GameStreamData}
import com.gcpm.stock.StockData
import com.gcpm.twitter.{RetweetType, TweetData}
import com.mongodb.MongoCredential._
import com.mongodb.{ConnectionString, ServerAddress}
import com.mongodb.connection.ServerSettings
import org.bson.codecs._
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.bson.codecs.configuration.{CodecProvider, CodecRegistries, CodecRegistry}
import org.bson.{BsonInvalidOperationException, BsonReader, BsonWriter}
import org.mongodb.scala.bson.DefaultBsonTransformers
import org.mongodb.scala.bson.codecs.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.{MongoClient, MongoClientSettings, MongoDatabase}

import scala.collection.JavaConverters._

class BigDecimalCodec extends Codec[BigDecimal] {
  override def encode(writer: BsonWriter, value: BigDecimal, encoderContext: EncoderContext): Unit = {
    writer.writeInt64(value.intValue())
  }

  override def decode(reader: BsonReader, decoderContext: DecoderContext): BigDecimal = reader.readInt64()

  override def getEncoderClass: Class[BigDecimal] = classOf[BigDecimal]
}

object RetweetTypeCodecProvider extends CodecProvider {
  def isCaseObjectEnum[T](clazz: Class[T]): Boolean = {
    clazz.isInstance(RetweetType.RETWEET) || clazz.isInstance(RetweetType.QUOTE) || clazz.isInstance(RetweetType.NONE)
  }

  override def get[T](clazz: Class[T], registry: CodecRegistry): Codec[T] = {
    if (isCaseObjectEnum(clazz)) {
      RetweetTypeCodec.asInstanceOf[Codec[T]]
    } else {
      null
    }
  }

  object RetweetTypeCodec extends Codec[RetweetType] {
    override def decode(reader: BsonReader, decoderContext: DecoderContext): RetweetType = {
      val theValue = reader.readString()
      theValue match {
        case "None" => RetweetType.NONE
        case "Quote" => RetweetType.QUOTE
        case "Retweet" => RetweetType.RETWEET
        case _ => throw new BsonInvalidOperationException(s"$theValue is an invalid value for a MyEnum object")
      }
    }

    override def encode(writer: BsonWriter, value: RetweetType, encoderContext: EncoderContext): Unit = {
      writer.writeString(value match {
        case RetweetType.NONE => "None"
        case RetweetType.QUOTE => "Quote"
        case RetweetType.RETWEET => "Retweet"
      })
    }

    override def getEncoderClass: Class[RetweetType] = RetweetType.getClass.asInstanceOf[Class[RetweetType]]
  }
}

trait Mongo extends EnvConfig with DefaultBsonTransformers {

  private val mongoClient = MongoClient(mongoUri)
  private val registry: CodecRegistry = CodecRegistries.fromCodecs(new BigDecimalCodec())

  private lazy val codecRegistry = fromRegistries(
    fromProviders(
      classOf[GameData],
      classOf[GameStreamData],
      classOf[TweetData],
      classOf[StockData]), DEFAULT_CODEC_REGISTRY, registry, CodecRegistries.fromProviders(RetweetTypeCodecProvider))
  lazy val database: MongoDatabase = mongoClient.getDatabase("gcpm").withCodecRegistry(codecRegistry)
}

object Mongo {
  private def date(from: Date, to: Date, target: Date): Boolean = {
    target.before(to) && target.after(from)
  }

  def dateFilter(dateFromOpt: Option[Date], dateToOpt: Option[Date], dateProp: TimeBasedData): Boolean = (dateFromOpt, dateToOpt) match {
    case (Some(dateFrom), Some(dateTo)) => date(dateFrom, dateTo, dateProp.getDate())
    case _ => true
  }
}