package com.gcpm

import java.text.SimpleDateFormat
import java.util.Date

import akka.http.scaladsl.unmarshalling.Unmarshaller
import org.mongodb.scala.bson.ObjectId
import spray.json.DefaultJsonProtocol

import scala.util.Try

trait MongoJsonSupport extends DefaultJsonProtocol{

  import spray.json._

  private val localIsoDateFormatter = new ThreadLocal[SimpleDateFormat] {
    override def initialValue() = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
  }

  private def dateToIsoString(date: Date) =
    localIsoDateFormatter.get().format(date)

  private def parseIsoDateString(date: String): Option[Date] =
    Try {
      localIsoDateFormatter.get().parse(date)
    }.toOption

  implicit object ObjectIdJsonFormat extends RootJsonFormat[ObjectId] {
    def write(c: ObjectId) = JsString(c.toHexString)

    def read(value: JsValue): ObjectId = value match {
      case JsString(c) => new ObjectId(c)
      case _ => deserializationError("Color expected")
    }
  }

  implicit object DateFormat extends JsonFormat[Date] {
    def write(date: Date) = JsString(dateToIsoString(date))

    def read(json: JsValue): Date = json match {
      case JsString(rawDate) =>
        parseIsoDateString(rawDate)
          .fold(deserializationError(s"Expected ISO Date format, got $rawDate"))(identity)
      case error => deserializationError(s"Expected JsString, got $error")
    }
  }

  protected val sdf = new SimpleDateFormat("yyyy-MM-dd")

  implicit val stringToDate: Unmarshaller[String, Date] = Unmarshaller.strict[String, Date](xx => sdf.parse(xx))


}
