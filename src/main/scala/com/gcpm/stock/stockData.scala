package com.gcpm.stock

import java.util.Date

import com.gcpm.gamedata.GraphGameData
import com.gcpm.{MongoJsonSupport, TimeBasedData}
import com.typesafe.scalalogging.LazyLogging
import spray.json.{JsObject, JsString, JsValue, RootJsonFormat}
import sun.reflect.generics.reflectiveObjects.NotImplementedException

case class StockData(
                      companyCode: String,
                      priceUsdOnClose: Double,
                      volume: Int,
                      date: Date
                    ) extends TimeBasedData {
  override def getDate(): Date = date
}

case class GraphStockData(
                          companyCode: String,
                          data: List[((Date, Date), Double, Int)],
                          groupedBy: String
                        )

case class StockDataList(seq: List[StockData])


trait StockDataJsonMappers extends MongoJsonSupport with LazyLogging {

  implicit val graphGameDataJsonFormat: RootJsonFormat[GraphStockData] = jsonFormat3(GraphStockData)
  implicit val stockDataJsonFormat: RootJsonFormat[StockData] = jsonFormat4(StockData)

  implicit object AlphaVantangeJsonFormat extends RootJsonFormat[StockDataList] {
    override def read(value: JsValue): StockDataList = {
      val x = value.asJsObject
      val companyCode = parseMetaData(x.fields.head._2)
      val others = x.fields.tail.head._2.asInstanceOf[JsObject].fields.map( parseTimeSeries).toList
      StockDataList(others.map(y => StockData(companyCode, y._1, y._2, y._3)))
    }

    override def write(obj: StockDataList): JsValue = throw new NotImplementedException()
  }

  private def parseTimeSeries(params: (String, JsValue)) = {
    try{
      logger.info(params.toString())
      val date = sdf.parse(params._1)
      val qq = params._2.asJsObject.getFields("4. close", "5. volume") match {
        case Seq(JsString(closePrice), JsString(volume)) => {
          (closePrice.toDouble, BigDecimal(volume))
        }
      }
      (qq._1, qq._2.intValue(), date)
    }catch {
      case ex: Exception =>
        logger.error(s"Failed to parse date: ${params}")
        throw ex
    }

  }

  private def parseMetaData(metaDataMap: JsValue) = {
    metaDataMap.asJsObject.getFields("2. Symbol") match {
      case Seq(JsString(symbol)) => symbol
    }
  }
}