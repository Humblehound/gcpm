package com.gcpm.stock

import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.Date

import akka.http.scaladsl.model.{HttpRequest, HttpResponse, Uri}
import akka.stream.scaladsl.Sink
import akka.util.ByteString
import com.gcpm.{AkkaConfig, HttpClient}
import com.typesafe.scalalogging.LazyLogging
import org.mongodb.scala.Completed

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class StockService(stockRepository: StockRepository, httpClient: HttpClient) extends AkkaConfig with StockDataJsonMappers with LazyLogging {

  private val alphaVantageApiKey = sys.env.getOrElse("ALPHA_VANTAGE_API_KEY", "TEST")
  private val alphaVantageBaseUrl = s"https://www.alphavantage.co/query?apikey=$alphaVantageApiKey"
  private val timeSeriesQuery = "&function=TIME_SERIES_DAILY"
  private val fullQuery = "&outputsize=full"

  private def company(code: String) = s"&symbol=$code"

  def getStockData(companyCodes: Iterable[String], dateFrom: Option[Date] = Option.empty, dateTo: Option[Date] = Option.empty): Future[Seq[StockData]] = {
    Future.sequence(companyCodes.map(companyCode => {
      for {
        _ <- makeSureDataIsUpToDate(companyCode)
        stockData <- stockRepository.getCompanyDataForOne(companyCode, dateFrom, dateTo)
      } yield stockData
    }))
      .map(_.flatten)
      .map(_.toSeq)
  }

  def getAverageStockData(companyCodes: Seq[String], dateFrom: Option[Date], dateTo: Option[Date], groupBy: String): Future[List[GraphStockData]] = {
    Future.sequence(companyCodes.map(makeSureDataIsUpToDate))
      .flatMap(_ => stockRepository.getCompanyData(companyCodes, dateFrom, dateTo))
      .map(_.groupBy(_.companyCode)).map(_.map {
      case (title, entries) =>
        val groupedByDays: Map[Date, Seq[StockData]] = entries.groupBy(x => Date.from(x.date.toInstant.truncatedTo(ChronoUnit.DAYS)))
        groupBy match {
          case "week" =>
            val averageViews = averagePriceByWeek(groupedByDays).sortBy(_._1)
            GraphStockData(title, averageViews, groupBy)
          case _ =>
            val averageViews = averageViewsByDay(groupedByDays).sortBy(_._1)
            GraphStockData(title, averageViews, groupBy)
        }
    }).map(_.toList)
  }

  private def averageViewsByDay(groupedByDays: Map[Date, Seq[StockData]]) =
    groupedByDays.map { case (date, stockData) => ((date, date),
      averageD(stockData.map(_.priceUsdOnClose).toList),
      averageI(stockData.map(_.volume).toList)
    )
    }.toList

  private def averagePriceByWeek(groupedByDays: Map[Date, Seq[StockData]]) = {
    // this method has the bold assumption that we always have data for each day...
    groupedByDays.toList.sortBy(_._1).grouped(7).map(list => {
      val firstDate = list.head._1
      val lastDate = list.last._1
      val avgPrice = averageD(list.flatMap(_._2).map(_.priceUsdOnClose))
      val avgVolume = averageI(list.flatMap(_._2).map(_.volume))
      ((firstDate, lastDate), avgPrice, avgVolume)
    }).toList
  }

  private def averageD(data: List[Double]): Double = {
    val sum = data.sum
    sum / data.length
  }

  private def averageI(data: List[Int]): Int = {
    val sum = data.sum
    sum / data.length
  }


  def makeSureDataIsUpToDate(companyCode: String): Future[Completed] = {
    stockRepository.getLatestRecordForCompany(companyCode).map {
      case Some(record) =>
        if (dataOlderThan2Days(record)) {
          logger.info(s"Existing $companyCode data older than 2 days, getting new records")
          updateStockData(stockDataQuery(companyCode), record)
        } else {
          logger.info(s"Data for $companyCode is up to date")
          Future.successful(Completed())
        }
      case None =>
        logger.info(s"No data for $companyCode found, fetching all")
        getAndSaveAllStockData(stockDataQuery(companyCode, full = true)).map(res => {
          logger.info("Successfully updated data")
          res
        })
    }.flatten
  }

  private def getAndSaveAllStockData(uri: Uri): Future[Completed] = {
    for {
      response <- httpClient.makeRequest(HttpRequest(uri = uri))
      stockDataList <- mapToEntity(response)
      result <- stockRepository.saveCompanyData(stockDataList)
    } yield result
  }

  private def updateStockData(uri: Uri, latestRecord: StockData): Future[Completed] = {
    for {
      response <- httpClient.makeRequest(HttpRequest(uri = uri))
      stockDataList <- mapToEntity(response)
      newRecords = getOnlyNewRecords(stockDataList, latestRecord)
      result <- {
        if (newRecords.nonEmpty) {
          stockRepository.saveCompanyData(newRecords)
        } else {
          Future.successful(Completed())
        }
      }
    } yield result
  }

  private def getOnlyNewRecords(newRecords: Seq[StockData], lastRecord: StockData): Seq[StockData] = {
    val newRecordsOnly = newRecords.filter(_.date.after(lastRecord.date))
    logger.info(s"${newRecordsOnly.length} new records fetched")
    newRecordsOnly
  }

  private def dataOlderThan2Days(stockData: StockData) = stockData.date.before(Date.from(Instant.now().minus(2, ChronoUnit.DAYS)))

  private def stockDataQuery(companyCode: String, full: Boolean = false): Uri = {
    val uri = Uri(alphaVantageBaseUrl) + timeSeriesQuery + company(companyCode)
    if (full) {
      uri + fullQuery
    } else {
      uri
    }
  }

  private def mapToEntity(res: HttpResponse) = {
    import spray.json._
    res.entity.dataBytes.runWith(Sink.fold(ByteString.empty)(_ ++ _))
      .map(_.utf8String)
      .map(_.parseJson.convertTo[StockDataList].seq)
  }
}
