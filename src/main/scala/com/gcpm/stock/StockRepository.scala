package com.gcpm.stock

import java.util.Date

import com.gcpm.Mongo
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Sorts._
import org.mongodb.scala.{Completed, MongoCollection, MongoDatabase}

import scala.concurrent.Future

class StockRepository(database: MongoDatabase) {

  protected lazy val collection: MongoCollection[StockData] = database.getCollection("stockData")

  def getCompanyDataForOne(companyCode: String, dateFrom: Option[Date] = Option.empty, dateTo: Option[Date] = Option.empty): Future[Seq[StockData]] = {
    val dateFilter = Mongo.dateFilter(dateFrom, dateTo, _)
    collection.find(equal("companyCode", companyCode)).sort(descending("date")).withFilter(dateFilter).toFuture()
  }

  def getCompanyData(companyCodes: Seq[String], dateFrom: Option[Date] = Option.empty, dateTo: Option[Date] = Option.empty): Future[Seq[StockData]] = {
    val dateFilter = Mongo.dateFilter(dateFrom, dateTo, _)
    collection.find(in("companyCode", companyCodes:_*)).withFilter(dateFilter).toFuture()
  }

  def getCompanyData(dateFrom: Option[Date], dateTo: Option[Date]): Future[Seq[StockData]] = {
    val dateFilter = Mongo.dateFilter(dateFrom, dateTo, _)
    collection.find().sort(descending("date")).withFilter(dateFilter).toFuture()
  }

  def getLatestRecordForCompany(companyCode: String): Future[Option[StockData]] = {
    collection.find(equal("companyCode", companyCode)).sort(descending("date")).headOption()
  }

  def saveCompanyData(stockData: Seq[StockData]): Future[Completed] = {
    collection.insertMany(stockData).toFuture()
  }

}
