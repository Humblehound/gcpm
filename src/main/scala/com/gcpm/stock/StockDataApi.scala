package com.gcpm.stock

import java.util.Date

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.server.Directives.{complete, get, parameters, pathPrefix, _}
import akka.http.scaladsl.server.Route

class StockDataApi(stockService: StockService) extends StockDataJsonMappers {
  val stockRoutes: Route =
    (pathPrefix("stock") & get) {
      path("average") {
        parameters('companyCode.*, 'dateFrom.as[Date].?, 'dateTo.as[Date].?, 'groupBy){
          (companyCodes, dateFrom, dateTo, groupBy) => complete(stockService.getAverageStockData(companyCodes.toSeq, dateFrom, dateTo, groupBy))
        }
      }
    }
}
