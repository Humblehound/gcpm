package com.gcpm

import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import ch.megard.akka.http.cors.scaladsl.CorsDirectives.cors
import ch.megard.akka.http.cors.scaladsl.settings.CorsSettings
import com.gcpm.gamedata.{GameDataApi, GameDataRepository, GameDataService}
import com.gcpm.stock.{StockDataApi, StockRepository, StockService}
import com.gcpm.twitter.TweetDataRepository
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.{ExecutionContext, Future}

object Main extends App with Mongo with AkkaConfig with LazyLogging {

  protected implicit val executor: ExecutionContext = system.dispatcher

  val httpClient = new HttpClient

  val stockRepository = new StockRepository(database = database)
  val stockService = new StockService(stockRepository, httpClient)
  val stockApi = new StockDataApi(stockService)

  val gameDataRepository = new GameDataRepository(database = database)
  val gameDataService = new GameDataService(gameDataRepository)
  val gameDataApi = new GameDataApi(gameDataRepository)

  val tweetDataRepository = new TweetDataRepository(database = database)

  private val settings = CorsSettings.defaultSettings.withAllowGenericHttpRequests(true)
  private val routes: Route = cors(settings) {
    gameDataApi.gameDataRoutes ~
      stockApi.stockRoutes
  }

  val bindingFuture = Http().bindAndHandle(handler = logRequestResult("log")(routes), interface = interface, port = port)
  logger.info(s"Server online at ${interface + " " + port}")
//
  val twitchPoll = gameDataService.pollGameStreamData()
  val all = Future.sequence(Seq(twitchPoll, bindingFuture))
}