package com.gcpm

import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}

import scala.concurrent.Future

class HttpClient extends AkkaConfig {

  private lazy val httpClient = Http()

  def makeRequest(httpRequest: HttpRequest): Future[HttpResponse] = httpClient.singleRequest(httpRequest)

}
