package com.gcpm.twitter

import java.util.Date

import com.danielasfregola.twitter4s.TwitterStreamingClient
import com.danielasfregola.twitter4s.entities.Tweet
import com.danielasfregola.twitter4s.entities.enums.Language
import com.danielasfregola.twitter4s.entities.streaming.CommonStreamingMessage
import com.danielasfregola.twitter4s.entities.streaming.common.{DisconnectMessage, LimitNotice}
import com.danielasfregola.twitter4s.http.clients.streaming.TwitterStream
import com.gcpm.{EnvConfig, Games}
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.{Await, Future}

class TwitterService(tweetDataRepository: TweetDataRepository, streamingClient: TwitterStreamingClient) extends LazyLogging with EnvConfig with TweetDataJsonMappers {

  def getTweets(tags: Seq[String], dateFrom: Option[Date], dateTo: Option[Date]): Future[Seq[TweetData]] = {
    tweetDataRepository.getTweets(tags, dateFrom, dateTo)
  }

  def pollGameTweetData(): Future[TwitterStream] = {
    if (twitterPollingActive) {
      logger.info("Twitter data polling active")
      val result = streamingClient.filterStatuses(tracks = Games.allTags, languages = Seq(Language.English)) {
        case dm: DisconnectMessage => {
          val ex = new StreamClosingException(dm)
          logger.error("Disconnect message received from twitter:",  ex)
          throw ex
        }
        case ln: LimitNotice => ln.limit
        case tweet: Tweet =>
//          val ex = new StreamClosingException(dm)
//          logger.error("Disconnect message received from twitter:",  ex)
          logger.info("THROWING")
          throw new StreamClosingException(null)
//          val td = TweetData.fromTweet(tweet)
//          logger.info(s"New tweet: ${td.toString}")
//          tweetDataRepository.saveTweetData(td).recover {
//            case ex: Exception => logger.error("Failed to save tweet: ", ex)
//          }
      }
      import scala.concurrent.duration._
      val t = Await.result(result, 30 seconds)
      null
    }
    else {
      logger.info("Twitter data polling not active")
//      Future.successful(None)
      null
    }
  }
}

class StreamClosingException(reason: CommonStreamingMessage) extends Throwable