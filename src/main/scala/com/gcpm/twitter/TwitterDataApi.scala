package com.gcpm.twitter

import java.util.Date

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.server.Directives.{complete, get, parameters, pathPrefix, _}
import akka.http.scaladsl.server.Route

class TwitterDataApi(twitterService: TwitterService) extends TweetDataJsonMappers {
  val tweetDataRoutes: Route =
    (pathPrefix("tweet") & get) {
      parameters('tag.*, 'dateFrom.as[Date].?, 'dateTo.as[Date].?) {
        (tags, dateFrom, dateTo) => complete(twitterService.getTweets(tags.toSeq, dateFrom, dateTo))
      }
    }
}
