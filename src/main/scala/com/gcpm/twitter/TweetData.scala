package com.gcpm.twitter

import java.util.Date

import com.danielasfregola.twitter4s.entities.{ExtendedTweet, Tweet}
import com.gcpm.twitter.RetweetType.RETWEET
import com.gcpm.{MongoJsonSupport, TimeBasedData, twitter}
import com.typesafe.scalalogging.LazyLogging
import spray.json.{JsString, JsValue, JsonFormat, RootJsonFormat}

case class TweetData(
                      createdAt: Date,
                      tweetId: Long,
                      text: String,
                      hashtags: Seq[String],
                      retweetType: RetweetType
                    ) extends TimeBasedData {
  override def getDate(): Date = createdAt
}

case object TweetData extends LazyLogging {
  def fromTweet(tweet: Tweet): TweetData = {
    (tweet.retweeted_status, tweet.quoted_status) match {
      case (Some(_), None) => buildTweetData(tweet.retweeted_status.get, RetweetType.RETWEET)
      case (None, Some(_)) => buildTweetData(tweet.quoted_status.get, RetweetType.QUOTE)
      case (None, None) => buildTweetData(tweet, RetweetType.NONE)
      case (Some(_), Some(_)) =>
        // This is clearly wrong, I need to figure out a better way to handle tweets
        buildTweetData(tweet.quoted_status.get, RetweetType.QUOTE)
    }
  }

  private def buildTweetData(tweet: Tweet, retweetType: RetweetType) = {
    val (fulltext, hashtags) = getFullTextAndHashtags(tweet)
    TweetData(Date.from(tweet.created_at), tweet.id, fulltext, hashtags, retweetType)
  }

  private def getFullTextAndHashtags(tweet: Tweet): (String, Seq[String]) = {
    if (tweet.truncated) {
      tweet.extended_tweet.map(exTweet => (exTweet.full_text, getHashtags(exTweet))).get
    } else {
      (tweet.text, getHashtags(tweet))
    }
  }

  private def getHashtags(tweet: Tweet) = tweet.entities.map(_.hashtags.map(_.text)).getOrElse(Seq())

  private def getHashtags(tweet: ExtendedTweet) = tweet.entities.map(_.hashtags.map(_.text)).getOrElse(Seq())

}


trait TweetDataJsonMappers extends MongoJsonSupport {

  implicit object RetweetTypeFormat extends JsonFormat[RetweetType] {
    override def write(rt: RetweetType) = JsString(rt.toString)

    override def read(json: JsValue): RetweetType = ???
  }

  implicit val tweetDataJsonFormat: RootJsonFormat[TweetData] = jsonFormat5(TweetData.apply)


}

sealed trait RetweetType

object RetweetType {

  case object RETWEET extends RetweetType

  case object QUOTE extends RetweetType

  case object NONE extends RetweetType

  def fromString(value: String): Option[RetweetType] = {
    Vector(RETWEET, QUOTE, NONE).find(_.toString == value)
  }
}

