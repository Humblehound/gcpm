package com.gcpm.twitter

import java.util.Date

import com.gcpm.Mongo
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Sorts.descending
import org.mongodb.scala.{Completed, MongoCollection, MongoDatabase}

import scala.concurrent.Future

class TweetDataRepository(database: MongoDatabase) {
  def getTweets(tags: Seq[String], dateFrom: Option[Date], dateTo: Option[Date]): Future[Seq[TweetData]] = {
    val lowerCaseTags = tags.map(_.toLowerCase)
    val dateFilter = Mongo.dateFilter(dateFrom, dateTo, _)

    collection.find(in("hashtags", lowerCaseTags: _*)).sort(descending("date")).withFilter(dateFilter).toFuture()
  }

  protected lazy val collection: MongoCollection[TweetData] = database.getCollection("tweetData")

  def saveTweetData(tweetData: TweetData): Future[Completed] = {
    val normalizedTweet = tweetData.copy(hashtags = tweetData.hashtags.map(_.toLowerCase))
    collection.insertOne(normalizedTweet).toFuture()
  }

}
