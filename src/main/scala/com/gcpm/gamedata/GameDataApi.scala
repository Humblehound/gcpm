package com.gcpm.gamedata

import java.util.Date

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.server.Directives.{complete, get, path, _}
import akka.http.scaladsl.server.Route

class GameDataApi(gameDataRepository: GameDataRepository) extends GameDataJsonMappers {

  val gameDataRoutes: Route =
    (path("games") & get) {
      complete(gameDataRepository.getAll)
    } ~
      (pathPrefix("game") & get) {
        path("average") {
          parameters('dateFrom.as[Date].?, 'dateTo.as[Date].?, 'groupBy){
            (dateFrom, dateTo, groupBy) => complete(gameDataRepository.getAverageViewersForAllGames(dateFrom, dateTo, groupBy))
          }
        } ~
        parameters('gameName) {
          gameName => complete(gameDataRepository.getAverageViewersForGameByDate(gameName))
        }
      }
}
