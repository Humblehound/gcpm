package com.gcpm.gamedata

import java.net.URLEncoder
import java.util.Date

import akka.Done
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model.{HttpRequest, HttpResponse, Uri}
import akka.stream.scaladsl.{Sink, Source}
import akka.util.ByteString
import com.gcpm.{AkkaConfig, EnvConfig, Games}
import com.typesafe.scalalogging.LazyLogging
import org.mongodb.scala.Completed
import org.mongodb.scala.bson.ObjectId

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Success}

class GameDataService(gameDataRepository: GameDataRepository) extends AkkaConfig with EnvConfig with LazyLogging with GameDataJsonMappers {
  private val twitchUrl = "https://api.twitch.tv/kraken/streams/summary?game="

  def pollGameStreamData(): Future[Done] = {
    if (twitchPollingActive) {
      logger.info("Twitch data polling active")
      Source
        .tick(0.seconds, 1.hour, Games.allGames)
        .map(fetchGameStreamData)
        .runWith(Sink.ignore)
    } else {
      logger.info("Twitch data polling not active")
      Future.successful(Done)
    }
  }

  private def fetchGameStreamData(gameList: Seq[Game]): Unit = {
    logger.info(s"Fetching stream data for all games")
    logger.debug(s"Games list: ${gameList.map(_.twitchGameTitle)}")
    Future.sequence(gameList.map(game => {
      val uri = Uri(twitchUrl) + URLEncoder.encode(game.twitchGameTitle, "UTF-8")
      ((for {
        response <- getData(uri)
        gst <- mapToEntity(response)
        result <- save(game, gst)
      } yield result) map (_ => {
        logger.debug(s"Fetched and saved game stream data for ${game.twitchGameTitle}")
      })).recover {
        case ex: Exception =>
          logger.error(s"Failed to get game stream data for ${game.twitchGameTitle}", ex)
          throw ex
      }
    })).onComplete {
      case Success(_) => logger.info(s"Successfully finished fetching all game stream data")
      case Failure(ex) => logger.error(s"There was a problem fetching game stream data for $gameList", ex)
    }
  }

  private def save(game: Game, gameStreamData: GameStreamData): Future[Completed] = {
    val gameData = GameData(new ObjectId(), game.twitchGameTitle, gameStreamData, new Date())
    gameDataRepository.save(gameData).toFuture()
  }

  private def getData(uri: Uri) = Http().singleRequest(
    HttpRequest(uri = uri)
      .withHeaders(RawHeader("Client-ID", twitchApiKey), RawHeader("Accept", "application/vnd.twitchtv.v5+json"))
  )

  private def mapToEntity(res: HttpResponse) = {
    import spray.json._
    res.entity.dataBytes.runWith(Sink.fold(ByteString.empty)(_ ++ _))
      .map(_.utf8String)
      .map(_.parseJson.convertTo[GameStreamData])
  }


}
