package com.gcpm.gamedata

import java.util.Date

import com.gcpm.{MongoJsonSupport, TimeBasedData}
import org.mongodb.scala.bson.ObjectId
import spray.json.RootJsonFormat


final case class Game(twitchGameTitle: String, tags: Seq[String])

final case class GameStreamData(channels: Int, viewers: Int)

final case class GameData(_id: ObjectId,
                          title: String,
                          gameStreamData:
                          GameStreamData,
                          date: Date) extends TimeBasedData {
  override def getDate(): Date = date
}

case class GraphGameData(
                                title: String,
                                data: List[((Date, Date), Int)],
                                groupedBy: String
                              )


trait GameDataJsonMappers extends MongoJsonSupport {
  implicit val gameStreamDataFormat: RootJsonFormat[GameStreamData] = jsonFormat2(GameStreamData)
  implicit val gameDataJsonFormat: RootJsonFormat[GameData] = jsonFormat4(GameData)
  implicit val graphGameDataJsonFormat: RootJsonFormat[GraphGameData] = jsonFormat3(GraphGameData)
}