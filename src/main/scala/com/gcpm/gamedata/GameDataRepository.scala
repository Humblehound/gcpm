package com.gcpm.gamedata

import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.Date

import com.gcpm.Mongo
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.{Completed, MongoCollection, MongoDatabase, SingleObservable}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class GameDataRepository(database: MongoDatabase) extends GameDataJsonMappers {

  private lazy val collection: MongoCollection[GameData] = database.getCollection("gameData")

  def getAll: Future[Seq[GameData]] = {
    collection.find().collect().toFuture()
  }

  def getByDates(dateFrom: Instant, dateTo: Instant): Future[Seq[GameData]] = {
    collection.find(
      and(
        gte("date", dateFrom),
        lte("date", dateTo)
      )
    ).collect().toFuture()
  }

  def getAverageViewersForAllGames(dateFromOpt: Option[Date], dateToOpt: Option[Date], groupBy: String): Future[List[GraphGameData]] = {
    collection.find
      .withFilter(Mongo.dateFilter(dateFromOpt, dateToOpt, _))
      .collect().toFuture()
      .map(_.groupBy(_.title)).map(_.map {
      case (title, entries) =>
        val groupedByDays: Map[Date, Seq[GameData]] = entries.groupBy(x => Date.from(x.date.toInstant.truncatedTo(ChronoUnit.DAYS)))
        groupBy match {
          case "week" =>
            val averageViews = averageViewsByWeek(groupedByDays).sortBy(_._1)
            GraphGameData(title, averageViews, groupBy)
          case _ =>
            val averageViews = averageViewsByDay(groupedByDays).sortBy(_._1)
            GraphGameData(title, averageViews, groupBy)
        }
    }).map(_.toList)
  }

  private def averageViewsByDay(groupedByDays: Map[Date, Seq[GameData]]) =
    groupedByDays.map { case (date, gameData) => ((date, date), average(gameData.map(_.gameStreamData.viewers).toList)) }.toList

  private def averageViewsByWeek(groupedByDays: Map[Date, Seq[GameData]]) = {
    // this method has the bold assumption that we always have data for each day...
    groupedByDays.toList.sortBy(_._1).grouped(7).map(list => {
      val firstDate = list.head._1
      val lastDate = list.last._1
      val avg = average(list.flatMap(_._2).map(_.gameStreamData.viewers))
      ((firstDate, lastDate), avg)
    }).toList
  }

  private def average(data: List[Int]): Int = {
    val sum = data.sum
    sum / data.length
  }

  def getAverageViewersForGameByDate(title: String): Future[List[(Date, Int)]] = {
    val tt: Future[Seq[GameData]] = collection.find(
      equal("title", title)
    ).collect().toFuture()

    val grouped = tt.map(_.groupBy(x => Date.from(x.date.toInstant.truncatedTo(ChronoUnit.DAYS))))
    grouped.map(_.map {
      case (date, entries) =>
        val data = entries.map(_.gameStreamData.viewers)
        val sum = data.sum
        val average = sum / data.length
        (date, average)
    }.toList.sortBy(_._1))
  }

  def save(gameData: GameData): SingleObservable[Completed] = {
    collection.insertOne(gameData)
  }
}
