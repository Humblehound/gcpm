package com.gcpm

import com.typesafe.config.ConfigFactory


trait EnvConfig {
  private lazy val config = ConfigFactory.load()

  private val httpConfig = config.getConfig("server")
  protected val interface: String = sys.env.getOrElse("INTERFACE", httpConfig.getString("interface"))
  protected val port: Int = sys.env.get("PORT").map(_.toInt).getOrElse(httpConfig.getInt("port"))

  protected val twitchApiKey = sys.env.getOrElse("TWITCH_API_KEY", "TEST")
  protected val twitchPollingActive = sys.env.get("TWITCH_POLL").exists(_.toBoolean)

  protected val twitterPollingActive = sys.env.get("TWITTER_POLL").exists(_.toBoolean)

  protected val mongoUri = sys.env.getOrElse("MONGO_URI", config.getString("mongo.uri"))
}