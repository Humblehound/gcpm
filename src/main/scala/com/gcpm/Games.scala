package com.gcpm

import com.gcpm.gamedata.Game

object Games {
  val blizzardGames: Seq[Game] = Seq(
    Game("World of Warcraft", tags =
      Seq(
        "#wowclassic",
        "#classicwow",
        "#vanillawow",
        "#wowvanilla",
        "#worldofwarcraftclassic",
        "#worldofwarcraft ",
        "#warcraft",
        "#blizzardentertainment"
      )),
    Game("Overwatch", tags = Seq(
      "#overwatch",
      "#overwatchleague",
      "#OWL2019")),
    Game("Starcraft II", tags = Seq(
      "Starcraft2",
      "StarcraftII",
      "#SGC2019",
      "SC2")),
    Game("Warcraft III: The Frozen Throne", Seq(
      "#warcraft3",
      "#wciii",
      "#wc3")),
    Game("Diablo III: Reaper of Souls", Seq(
      "#diablo3",
      "#diablo"))
  )

  val activisionGames: Seq[Game] = Seq(
    Game("Destiny 2", Seq(
      "#Destiny2",
      "#DestinyTheGame")),
    Game("Call of Duty: Black Ops 4", Seq(
      "#CallofDutyBlackOps4",
      "#CallOfDuty",
      "#CallOfDutyLeague"
    ))
  )

  val eaGames: Seq[Game] = Seq(
    Game("Anthem", Seq(
      "#Anthem",
      "#AnthemGame"
    )),
    Game("Apex Legends", Seq(
      "#ApexLegends",
      "#Apex",
      "#ApexClips"
    )),
    Game("Battlefield V", Seq(
      "#BattlefieldV",
      "#Battlefield5",
      "#Battlefield",
      "#bf5",
      "#bfv"
    )),
    Game("Fifa 19", Seq(
      "#fifa19",
      "#FUT19",
      "#FUT",
      "#TOTS"
    )),
    Game("The SIms 4", Seq(
      "#thesims",
      "#thesims4",
      "#sims4",
      "#sims"
    ))
  )

  val companyTags: Seq[String] = Seq("#blizzard", "#blizzardentertainment", "#activisionblizzard", "#ea", "#eagames")

  val allGames: Seq[Game] = blizzardGames ++ activisionGames ++ eaGames

  val allTags: Seq[String] = allGames.flatMap(_.tags) ++ companyTags

}
