package com.gcpm

import java.text.SimpleDateFormat
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.Date

import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpResponse, StatusCodes}
import com.gcpm.stock.StockData
import spray.json.{JsObject, JsString}

object TestUtil {

  val sdf = new SimpleDateFormat("yyyy-MM-dd")

  def mapStockDataToHttpResponse(stockDataList: Seq[StockData]): HttpResponse = {
    val company = stockDataList.head.companyCode
    val mapped = stockDataList.map(mapEntryToTimeSeries)
    val data = JsObject(
      "Meta Data" -> JsObject("2. Symbol" -> JsString(company)),
      "Time Series (Daily" -> JsObject(mapped: _*)
    )
    HttpResponse(StatusCodes.OK, entity = HttpEntity(ContentTypes.`application/json`, data.toString()))
  }

  private def mapEntryToTimeSeries(stockData: StockData) = {
    sdf.format(stockData.date) -> JsObject(
      "4. close" -> JsString(stockData.priceUsdOnClose.toString),
      "5. volume" -> JsString(stockData.volume.toString)
    )
  }

  def currentDate(minusDays: Int = 0): Date = {
    sdf.parse(sdf.format(Date.from(Instant.now().minus(minusDays, ChronoUnit.DAYS))))
  }
}
