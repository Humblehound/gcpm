package com.gcpm.twitter

import org.scalamock.scalatest.MockFactory
import org.scalatest.WordSpec
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}

class TwitterServiceSpec extends WordSpec with MockFactory with ScalaFutures with TweetDataJsonMappers {

  private implicit val defaultPatience = PatienceConfig(timeout = Span(5, Seconds), interval = Span(500, Millis))

//  private val twitterStreamingClient: TwitterStreamingClient = mock[TwitterStreamingClient]


  "TwitterService" should {

    "retry immediately upon disconnect message" in {
    }

    "retry with backoff upon  " in {

    }

    "fail the future after disconnect message" in {

    }

  }



}
