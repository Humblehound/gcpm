package com.gcpm.stock

import com.gcpm.TestUtil.currentDate
import com.gcpm.{GameCompany, HttpClient, TestUtil}
import org.mongodb.scala.Completed
import org.scalamock.scalatest.MockFactory
import org.scalatest.WordSpec
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}

import scala.concurrent.Future

class StockServiceSpec extends WordSpec with MockFactory with ScalaFutures with StockDataJsonMappers {

  private implicit val defaultPatience = PatienceConfig(timeout = Span(5, Seconds), interval = Span(500, Millis))

  private val stockRepository: StockRepository = mock[StockRepository]
  private val httpClient = stub[HttpClient]
  private val stockService = new StockService(stockRepository, httpClient)
  private val companyCode = GameCompany.eaGames

  "StockService" should {

    "return stock data if they're up to date" in {

      val stockData = Seq(StockData(companyCode, 1.0d, 1, currentDate()))

      (stockRepository.getLatestRecordForCompany _).expects(companyCode).returns(Future.successful(Some(stockData.head))).once
      (stockRepository.getCompanyDataForOne _).expects(companyCode, *, *).returns(Future.successful(stockData)).once

      assert(stockService.getStockData(Seq(companyCode)).futureValue == stockData)
    }

    "get all stock data if none are found" in {

      val stockData = Seq(
        StockData(companyCode, 1.0d, 1, currentDate()),
        StockData(companyCode, 2.0d, 2, currentDate(2)),
        StockData(companyCode, 3.0d, 3, currentDate(3)),
      )

      val httpResponse = TestUtil.mapStockDataToHttpResponse(stockData)

      inSequence {
        (stockRepository.getLatestRecordForCompany _).expects(companyCode).returns(Future.successful(None)).once
        (httpClient.makeRequest _).when(*).returning(Future.successful(httpResponse)).once
        (stockRepository.saveCompanyData _).expects(*).returns(Future.successful(Completed())).once
        (stockRepository.getCompanyDataForOne _).expects(companyCode, *, *).returns(Future.successful(stockData)).once

      }

      assert(stockService.getStockData(Seq(companyCode)).futureValue == stockData)
    }

    "update stock data if out of date and return updated values" in {

      val oldStockData = Seq(
        StockData(companyCode, 3.0d, 3, currentDate(3)),
      )

      val newStockData = Seq(
        StockData(companyCode, 1.0d, 1, currentDate(1)),
        StockData(companyCode, 2.0d, 2, currentDate(2)),
        StockData(companyCode, 3.0d, 3, currentDate(3)),
      )

      val httpResponse = TestUtil.mapStockDataToHttpResponse(newStockData)

      inSequence {
        (stockRepository.getLatestRecordForCompany _).expects(companyCode).returns(Future.successful(Some(oldStockData.head))).once
        (httpClient.makeRequest _).when(*).returning(Future.successful(httpResponse)).once
        (stockRepository.saveCompanyData _).expects(*).returns(Future.successful(Completed())).once
        (stockRepository.getCompanyDataForOne _).expects(companyCode, *, *).returns(Future.successful(newStockData)).once
      }

      assert(stockService.getStockData(Seq(companyCode)).futureValue == newStockData)
    }
    "insert only new records into the database" in {

      val oldStockData = Seq(
        StockData(companyCode, 1.0d, 3, currentDate(3)),
      )

      val newStockData = Seq(
        StockData(companyCode, 1.0d, 1, currentDate(1)),
        StockData(companyCode, 2.0d, 2, currentDate(2)),
        StockData(companyCode, 3.0d, 3, currentDate(3)),
      )

      val httpResponse = TestUtil.mapStockDataToHttpResponse(newStockData)

      val newDataOnly = newStockData.filter(_.date.after(oldStockData.head.date)).sortBy(_.date)

      inSequence {
        (stockRepository.getLatestRecordForCompany _).expects(companyCode).returns(Future.successful(Some(oldStockData.head))).once
        (httpClient.makeRequest _).when(*).returning(Future.successful(httpResponse)).once
        (stockRepository.saveCompanyData _).expects(newDataOnly).returns(Future.successful(Completed())).once
        (stockRepository.getCompanyDataForOne _).expects(companyCode, *, *).returns(Future.successful(newStockData)).once
      }

      assert(stockService.getStockData(Seq(companyCode)).futureValue == newStockData)
    }

    "return stock data filtered by dates" in {
      val stockData = Seq(
        StockData(companyCode, 1.0d, 1, currentDate(1)),
        StockData(companyCode, 2.0d, 2, currentDate(2)),
        StockData(companyCode, 3.0d, 3, currentDate(3)),
        StockData(companyCode, 4.0d, 3, currentDate(4)),
        StockData(companyCode, 5.0d, 3, currentDate(5)),
      )

      val beforeDate = Some(currentDate())
      val afterDate = Some(currentDate(minusDays = 4))

      val stockDataFiltered = stockData.filter(stockData => stockData.date.before(beforeDate.get) && stockData.date.after(afterDate.get))

      (stockRepository.getLatestRecordForCompany _).expects(companyCode).returns(Future.successful(Some(stockData.head))).once
      (stockRepository.getCompanyDataForOne _).expects(companyCode, beforeDate, afterDate).returns(Future.successful(stockDataFiltered)).once

      assert(stockService.getStockData(Seq(companyCode), beforeDate, afterDate).futureValue == stockDataFiltered)
    }
  }
}
